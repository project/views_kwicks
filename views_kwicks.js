
(function ($) {
  Drupal.behaviors.viewsKwicks = {
    attach: function (context, settings) {
      if (settings.views_kwicks.views) {
        $.each(settings.views_kwicks.views, function(key, instance) {
          $(key + " .kwicks").kwicks(instance);
        });
      }
    }
  };
}(jQuery));
