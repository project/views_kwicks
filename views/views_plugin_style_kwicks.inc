<?php

/**
 * @file
 * Contains the kwicks style plugin.
 */

/**
 * Style plugin to render each item in a kwicks format
 *
 * @ingroup views_style_plugins
 */
class views_plugin_style_kwicks extends views_plugin_style {
  /**
   * Set default options
   */
  function option_definition() {
    $options = parent::option_definition();
    
    $options['wrapper_class'] = array('default' => 'views-kwicks');
    $options['class'] = array('default' => '');
    
    $options['kwicks_pane_width'] = array('default' => '200');
    $options['kwicks_pane_height'] = array('default' => '300');
    
    $options['kwicks_minmax'] = array('default' => 'min');
    $options['kwicks_min_value'] = array('default' => '100');
    $options['kwicks_max_value'] = array('default' => '400');
    
    $options['kwicks_vertical'] = array('default' => FALSE);
    $options['kwicks_sticky'] = array('default' => FALSE);
    
    $options['kwicks_duration'] = array('default' => '200');
    $options['kwicks_spacing'] = array('default' => '0');
    
    return $options;
  }
  
  /**
   * Render the option form.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['wrapper_class'] = array(
      '#title' => t('Wrapper class'),
      '#description' => t('The class to provide on the wrapper, outside the list.'),
      '#type' => 'textfield',
      '#size' => '30',
      '#default_value' => $this->options['wrapper_class'],
    );
    $form['class'] = array(
      '#title' => t('List class'),
      '#description' => t('The class to provide on the list element itself.'),
      '#type' => 'textfield',
      '#size' => '30',
      '#default_value' => $this->options['class'],
    );
    
    $form['kwicks_pane_width'] = array(
      '#type' => 'textfield',
      '#title' => t('Pane Width'),
      '#description' => t('The initial width of a kwick pane.'),
      '#default_value' => $this->options['kwicks_pane_width'],
      '#element_validate' => array('element_validate_integer_positive'),
    );
    $form['kwicks_pane_height'] = array(
      '#type' => 'textfield',
      '#title' => t('Pane Height'),
      '#description' => t('The initial height of a kwick pane.'),
      '#default_value' => $this->options['kwicks_pane_height'],
      '#element_validate' => array('element_validate_integer_positive'),
    );
    
    $form['kwicks_minmax'] = array(
      '#type' => 'select',
      '#title' => t('Min or Max'),
      '#description' => t('Which sizing measurement should we use.? We can not use both.'),
      '#options' => array(
        'min' => t('Min'),
        'max' => t('Max'),
      ),
      '#default_value' => $this->options['kwicks_minmax'],
    );
    $form['kwicks_min_value'] = array(
      '#type' => 'textfield',
      '#title' => t('Min Size'),
      '#description' => t('The width or height (in pixels) of a fully collapsed kwick pane. If vertical is set to true, then min refers to the height - otherwise it is the width.'),
      '#default_value' => $this->options['kwicks_min_value'],
      '#element_validate' => array('element_validate_integer'),
    );
    $form['kwicks_max_value'] = array(
      '#type' => 'textfield',
      '#title' => t('Max Size'),
      '#description' => t('The width or height (in pixels) of a fully expanded kwick pane. If vertical is set to true, then max refers to the height - otherwise it is the width.'),
      '#default_value' => $this->options['kwicks_max_value'],
      '#element_validate' => array('element_validate_integer'),
    );
    
    $form['kwicks_vertical'] = array(
      '#type' => 'checkbox',
      '#title' => t('Vertical Panes'),
      '#default_value' => $this->options['kwicks_vertical'],
    );
    
    $form['kwicks_sticky'] = array(
      '#type' => 'checkbox',
      '#title' => t('Sticky Panes'),
      '#description' => t('One kwicks pane will always be expanded if true.'),
      '#default_value' => $this->options['kwicks_sticky'],
    );
    
    $form['kwicks_duration'] = array(
      '#type' => 'textfield',
      '#title' => t('Easing Duration'),
      '#description' => t('The number of milliseconds required for each animation to complete.'),
      '#default_value' => $this->options['kwicks_duration'],
      '#element_validate' => array('element_validate_integer_positive'),
    );
    
    $form['kwicks_spacing'] = array(
      '#type' => 'textfield',
      '#title' => t('Pane Spacing'),
      '#description' => t('The width (in pixels) separating each kwick pane.'),
      '#default_value' => $this->options['kwicks_spacing'],
      '#element_validate' => array('element_validate_integer'),
    );
  }
}
