<?php

/**
 * @file
 * Views plugin definition.
 */

/**
 * Implements hook_views_plugins().
 * Adds Kwicks style to views UI.
 */
function views_kwicks_views_plugins() {
  $path = drupal_get_path('module', 'views_kwicks');
  return array(
    'style' => array(
      'views_kwicks'      => array(
        'title'            => t('Kwicks'),
        'help'             => t('Displays rows in a jQuery Kwicks format.'),
        'handler'          => 'views_plugin_style_kwicks',
        'theme'            => 'views_views_kwicks_style',
        'theme path'       => $path . '/views',
        'path'             => $path . '/views',
        'uses row plugin'  => TRUE,
        'uses fields'      => TRUE,
        'uses options'     => TRUE,
        //'uses grouping'    => FALSE,
        //'type'             => 'feed',
        'type'             => 'normal',
        //'help_topic'     => 'style-kwicks',
        'even empty'     => TRUE,
      ),
    ),
  );
}
